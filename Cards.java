import java.util.Random;

class GlobalVariables {
    int[] cards = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}; // 52
                //{0, 1, 2, 3, 4, 5, 6, 7, 8,  9,  10, 11, 12, 13,14,15,16,17,18,19,20,21, 22, 23, 24, 25, 26,27,28,29,30,31,32,33,34, 35, 36, 37, 38, 39,40,41,42,43,44,45,46,47, 48, 49, 50, 51}

    String[] cardsNames =
                  {"S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "S11", "S12", "S13", "S14", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10", "C11", "C12", "C13", "C14", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9", "H10", "H11", "H12", "H13", "H14", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10", "D11", "D12", "D13", "D14"}; // 52
                 //{0,    1,    2,    3,    4,    5,    6,    7,    8,     9,     10,    11,    12,    13,   14,   15,   16,   17,   18,   19,   20,   21,    22,    23,    24,    25,    26,   27,   28,   29,   30,   31,   32,   33,   34,    35,    36,    37,    38,    39,   40,   41,   42,   43,   44,   45,   46,   47,    48,    49,    50,    51}

    int availableCards[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //52

    String users[] = {"User A", "User B", "User C", "User D"};

    int userCards[] = new int[12]; //this array will save 3 cards for each 4 users

}

class Cards {

    static GlobalVariables gv = new GlobalVariables();
    public static void main(String args[]){

        distributeCardsToUsers();

//        test(); if you want to test the code with custom input then kindle use this function

        displayUserCards();

        System.out.print("\n");

        if(checkHighestPossibleCombination()) {
            System.out.println("Won by highest combination");
            return;
        }
        System.out.println("No highest combination");


        if(checkSequence()) { // it will check sequence of 2 to 14
            System.out.println("Won by highest sequence");
            return;
        }


        convert14to1();
        if(checkSequence()) { // it will check sequence of 1 to 13
            System.out.println("Won by highest sequence.");
            return;
        }
        convert1to14();
        System.out.println("No highest sequence");


        if(checkHighestPairOfCards()) {
            System.out.println("Won by highest pair of cards");
            return;
        }
        System.out.println("No highest pair of cards");


        int[] usersTopCardStatus = checkTopCard(keepOnlyTopCards(gv.userCards));
        if(usersTopCardStatus == null)
            System.out.println("won by top card");
        else {
            System.out.println("No top card");
            RandomCardsFromDeck(usersTopCardStatus);
        }
    }

    static Boolean RandomCardsFromDeck(int[] usersTopCardStatus){

        String userStr = "";
        for(int user=0;user<usersTopCardStatus.length;user++){
            if(usersTopCardStatus[user]!=0)
                userStr += gv.users[user] + "  ";
        }
        System.out.print("\nThese users  "+userStr+"are having same Top card. So, fight between them\n");

        int[] usersRandomCard = {0, 0, 0, 0};
        for(int user=0;user<usersTopCardStatus.length;user++){
            if(usersTopCardStatus[user]!=0){
                usersRandomCard[user] = pickRandomCardFromDeck();
                System.out.println(gv.users[user]+ " random card " +usersRandomCard[user]);
            }
        }

        return checkTopCardFromRandomCards(keepOnlyTopCards(usersRandomCard));
    }

    static Boolean checkTopCardFromRandomCards(int cards[]) {
        int winner = -1;
        int totTopCards = 0;
        int[] usersTopCardStatus = {0, 0, 0, 0};

        for(int user=0;user<cards.length;user++){
            if(cards[user] != 0) {
                usersTopCardStatus[user] = 1;
                winner = user;
                totTopCards++;
            }
        }

        if(totTopCards == 1)
            if(checkWinner(winner)) {
                System.out.println("won by random top card");
                return true;
            }

        return RandomCardsFromDeck(usersTopCardStatus);
    }

    static int pickRandomCardFromDeck(){
        Random r = new Random();

        int randomNo = r.nextInt(gv.cards.length);
        if(gv.availableCards[randomNo] == 0) {
            gv.availableCards[randomNo] = 1;
            return gv.cards[randomNo];
        }
        return pickRandomCardFromDeck();
    }

    static int[] checkTopCard(int cards[]) { // this will return null when there only one top card. Else top card status of the user
        int card = 0;
        int user = 0;
        int winner = -1;
        int totTopCards = 0;
        int[] usersTopCardStatus = {0, 0, 0, 0};

        while(user != gv.users.length){
            if(cards[card] != 0 || cards[card+1] != 0 || gv.userCards[card+2] != 0) {
                usersTopCardStatus[user] = 1;
                winner = user;
                totTopCards++;
            }

            user++;
            card = card+3;
        }

        if(totTopCards == 1)
            if(checkWinner(winner)) {
                return null;
            }

        return usersTopCardStatus;
    }

    static Boolean checkHighestPairOfCards() {

        int card = 0;
        int user = 0;
        int winner = -1;
        int winnerCard = 0;

        while(user != gv.users.length){
            if(gv.userCards[card] == gv.userCards[card+1]){
                if(winnerCard < gv.userCards[card]){
                    winner = user;
                    winnerCard = gv.userCards[card];
                }
            } else if(gv.userCards[card] == gv.userCards[card+2]){
                if(winnerCard < gv.userCards[card]){
                    winner = user;
                    winnerCard = gv.userCards[card];
                }
            } else if(gv.userCards[card+1] == gv.userCards[card+2]){
                if(winnerCard < gv.userCards[card+1]){
                    winner = user;
                    winnerCard = gv.userCards[card+1];
                }
            }

            user++;
            card = card+3;
        }

        return checkWinner(winner);
    }

    static Boolean checkSequence() {

        int card = 0;
        int user = 0;
        int temp;
        int winner = -1;
        int winnerCard = 0;

        while(user != gv.users.length){
            //Below 3 if condition will ascending the user cards
            if(gv.userCards[card] > gv.userCards[card+1]) {
                temp = gv.userCards[card];
                gv.userCards[card] = gv.userCards[card+1];
                gv.userCards[card+1] = temp;
            }

            if(gv.userCards[card] > gv.userCards[card+2]) {
                temp = gv.userCards[card];
                gv.userCards[card] = gv.userCards[card+2];
                gv.userCards[card+2] = temp;
            }

            if(gv.userCards[card+1] > gv.userCards[card+2]) {
                temp = gv.userCards[card+1];
                gv.userCards[card+1] = gv.userCards[card+2];
                gv.userCards[card+2] = temp;
            }

            if(gv.userCards[card]+1 == gv.userCards[card+1] && gv.userCards[card]+2 == gv.userCards[card+2]) {
                if(winnerCard<gv.userCards[card]) {
                    winner = user;
                    winnerCard = gv.userCards[card];
                }
            }

            user++;
            card = card+3;
        }

        return checkWinner(winner);
    }

    static Boolean checkHighestPossibleCombination() {

        int card = 0;
        int user = 0;
        int winner = -1;
        int winnerCard = 0;

        while(user != gv.users.length){
            if(gv.userCards[card] == gv.userCards[card+1] && gv.userCards[card] == gv.userCards[card+2]) {

                if(winnerCard < gv.userCards[card]){
                    winner = user;
                    winnerCard = gv.userCards[card];
                }
            }

            user++;
            card = card+3;
        }

        return checkWinner(winner);
    }

    static void displayUserCards() {

        int card = 0;
        int user = 0;
        while(user != gv.users.length){
            System.out.println(gv.users[user]+": "+gv.userCards[card]+", "+ gv.userCards[card+1]+", "+ gv.userCards[card+2]);
//            System.out.println(gv.users[user]+": "+gv.cardsNames[gv.userCards[card]]+", "+ gv.cardsNames[gv.userCards[card+1]]+", "+ gv.cardsNames[gv.userCards[card+2]]);
            user++;
            card = card+3;

        }
    }

    static void distributeCardsToUsers() {

        Random r = new Random();

        int i = 0;
        int randomNo;

        while (i != gv.userCards.length) {
            randomNo = r.nextInt(gv.cards.length);
            if (gv.availableCards[randomNo] == 0) {
                gv.userCards[i] = gv.cards[randomNo];
                gv.availableCards[randomNo] = 1;
                i++;
            }
        }
    }

    static void test() {

////      Highest combination
        gv.userCards = new int[]{5,5,5, 2,2,2, 3,3,3, 4,4,4}; //User A is the winner
//        gv.userCards = new int[]{2,2,2, 5,5,5, 3,3,3, 4,4,4}; //User B is the winner
//        gv.userCards = new int[]{2,2,2, 3,3,3, 5,5,5, 4,4,4}; //User C is the winner
//        gv.userCards = new int[]{2,2,2, 3,3,3, 4,4,4, 5,5,5}; //User D is the winner
//
////      Highest sequence
//        gv.userCards = new int[]{2,4,3, 3,7,4, 4,9,5, 5,10,6}; //User A is the winner
//        gv.userCards = new int[]{8,7,6, 2,4,3, 4,9,5, 5,10,6}; //User A is the winner
//        gv.userCards = new int[]{2,4,3, 8,7,6, 4,9,5, 5,10,6}; //User B is the winner
//        gv.userCards = new int[]{2,14,3, 3,7,4, 4,9,5, 5,10,6}; //User A is the winner
//
////      Highest pair of cards
//        gv.userCards = new int[]{10,10,3, 14,3,14, 11,9,11, 4,10,6}; //User B is the winner
//        gv.userCards = new int[]{10,10,3, 4,3,14, 4,9,5, 5,10,6}; //User A is the winner
//
////      Top cards by number
//        gv.userCards = new int[]{10,9,3, 14,3,4, 8,9,4, 4,10,6}; //User B is the winner
//
////      It will trigger randomly chosen card
//        gv.userCards = new int[]{10,9,3, 10,3,4, 8,7,10, 4,9,6}; //No winner
//        gv.availableCards =  new int[]
//                      {0, 1, 0, 0, 0, 0, 0, 1,  1, 0,  0,  0,  0,  0, 1, 1, 0, 0, 1, 1, 0,  1,  0,  0,  0,  0, 0, 0, 1, 0, 1, 0, 0, 1,  1,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0,  0,  0,  0}; //52


    }

    static int[] keepOnlyTopCards(int cards[]) { // This function keeps only top and put 0 to remaining cards
        for (int i = 0; i < cards.length; i++) {
            for (int j = i + 1; j < cards.length; j++) {
                if (cards[i] > cards[j] && cards[i] != cards[j])
                    cards[j] = 0;
                else if (cards[i] != cards[j])
                    cards[i] = 0;
            }
        }
        return cards;
    }

    static void convert14to1() {
        for(int i=0;i < gv.userCards.length;i++){
            if(gv.userCards[i] == 14)
                gv.userCards[i] = 1;
        }
    }

    static void convert1to14() {
        for(int i=0;i < gv.userCards.length;i++){
            if(gv.userCards[i] == 1)
                gv.userCards[i] = 14;
        }
    }

    static Boolean checkWinner(int winner) {

        if(winner==-1)
            return false; // No winner
        else {
            System.out.println("\n"+gv.users[winner]+" is the winner");
            return true;
        }
    }
}